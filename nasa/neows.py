#!/usr/bin/python3
import requests

## Define NEOW URL
NEOURL = "https://api.nasa.gov/neo/rest/v1/feed?"

# this function grabs our credentials
# it is easily recycled from our previous script
def returncreds():
    ## first I want to grab my credentials
    with open("/home/student/nasa.creds", "r") as mycreds:
        nasacreds = mycreds.read()
    ## remove any newline characters from the api_key
    nasacreds = "api_key=" + nasacreds.strip("\n")
    return nasacreds

# this is our main function
def main():
    ## first grab credentials
    nasacreds = returncreds()
    
    startdate = "start_date="
    enddate = "end_date="
    startdate += input("Start date(YYYY-MM-DD):")
    enddate += input("End date(YYYY-MM-DD):")
    
    if not startdate.partition("=")[2]: 
        ## update the date below, if you like
        startdate += "2019-11-11"


    ## the value below is not being used in this
    ## version of the script
    # enddate = "end_date=END_DATE"

    # make a request with the request library
    neowrequest = requests.get(NEOURL + startdate + "&" + nasacreds)

    if enddate.partition("=")[2]:
        print("what is this " + str(enddate.partition("=")[2]))
        neowrequest = requests.get(NEOURL + startdate + "&" + enddate + "&" + nasacreds)

    print("URL = "+ neowrequest.url)
    # strip off json attachment from our response
    #neodata = neowrequest.json()

    ## display NASAs NEOW data
    #print(neodata)

if __name__ == "__main__":
    main()

