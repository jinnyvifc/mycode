<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href=".git/">.git/</a></li>
<li><a href="datacenter.json">datacenter.json</a></li>
<li><a href="dictrev01.py">dictrev01.py</a></li>
<li><a href="dictrev02.py">dictrev02.py</a></li>
<li><a href="dictrev03.py">dictrev03.py</a></li>
<li><a href="excelout/">excelout/</a></li>
<li><a href="galaxyguide.json">galaxyguide.json</a></li>
<li><a href="httpwork/">httpwork/</a></li>
<li><a href="iceAndFire01.py">iceAndFire01.py</a></li>
<li><a href="iceAndFire02.py">iceAndFire02.py</a></li>
<li><a href="iceAndFire03.py">iceAndFire03.py</a></li>
<li><a href="iceAndFire04.py">iceAndFire04.py</a></li>
<li><a href="iss/">iss/</a></li>
<li><a href="issloc/">issloc/</a></li>
<li><a href="jsontest/">jsontest/</a></li>
<li><a href="LICENSE">LICENSE</a></li>
<li><a href="listrev01.py">listrev01.py</a></li>
<li><a href="listrev02.py">listrev02.py</a></li>
<li><a href="listrev03.py">listrev03.py</a></li>
<li><a href="listrev04.py">listrev04.py</a></li>
<li><a href="listrev05.py">listrev05.py</a></li>
<li><a href="makejson01.py">makejson01.py</a></li>
<li><a href="makejson02.py">makejson02.py</a></li>
<li><a href="makejson03.py">makejson03.py</a></li>
<li><a href="makejson04.py">makejson04.py</a></li>
<li><a href="mygame01.py">mygame01.py</a></li>
<li><a href="nasa/">nasa/</a></li>
<li><a href="pandas/">pandas/</a></li>
<li><a href="pokemon/">pokemon/</a></li>
<li><a href="README.md">README.md</a></li>
<li><a href="rooms.json">rooms.json</a></li>
<li><a href="soap/">soap/</a></li>
<li><a href="swapi1.py">swapi1.py</a></li>
<li><a href="swapi2.py">swapi2.py</a></li>
<li><a href="xmen/">xmen/</a></li>
</ul>
<hr>
</body>
</html>
