#!/usr/bin/env python3
import http.client, json, argparse

def main(): 

    ## think of this as setting up the connection
    conn = http.client.HTTPConnection("localhost", 9021)
    if args.choice == '1':
        print(conn)
    else:

    ## Send an HTTP request and store the HTTP response
    ##    from our webserver
        conn.request('HEAD', '/')

    ## Returns just the response that has been associated with
    ##    the **conn** object.
        res = conn.getresponse()
    
    ## response status and the reason to the screen.
        print(res.status, res.reason)

    ## this time we'll issue GET
        conn.request('GET', '/')
    
    ## res is equal to the response associated with conn
        res = conn.getresponse()
    
    ## print the response status code and reason
        print(res.status, res.reason)
    
    ## page_data is all of the data associated with res 
        page_data = res.read().decode("utf-8")
        with open("http_readin.txt", "w") as f:
            f.write(page_data)
    ## this will point out all of the data associated with res
        print(page_data)
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--choice', help='Choose 1 to test a connection or 2 to perform a GET')
    args = parser.parse_args()
    main()

