#!/usr/bin/python3
"""Alta3 Research | <your name here>
   Using an HTTP GET to determine when the ISS will pass over head"""

# python3 -m pip install requests
import requests, time 

def main():
    """your code goes below here"""
    try :
        latitude = float(input("Current latitude: "))
        longitude = float(input("Current longitude: "))
        URL = "http://api.open-notify.org/iss-pass.json?lat=47.6&lon=-122.3"

        if latitude <= 90.0 and latitude >= -90.0 :
            if longitude <= 180.0 and longitude >= -180.0 :
                URL = f"http://api.open-notify.org/iss-pass.json?lat={latitude}&lon={longitude}"
            else:
                print("Longitude must be number between -180.0 and 180.0")
                print("The return value is query with default lat = 47.6 and lon = -122.3")
        else:
            print("Latitude must be number between -90.0 and 90.0")
            print("The return value is query with default lat = 47.6 and lon = -122.3")
        req = requests.get(URL)

        if req.status_code == 200:
            resp = req.json()
            hasOverhead = False
            for risetime in resp['response']:
                if resp['request']['datetime'] == risetime['risetime']:
                    hasOverhead = True;
                    overhead = time.ctime(risetime['risetime'])
                    duration = risetime['duration']
                    print("ISS will be overhead at " + str(overhead) + " for " + str(duration) + " seconds. ")
            if hasOverhead == False:
                print("No overhead will be occurred.")
    except ValueError as e:
        print("Latitude and Longitude must be float or integer.") 
    # stuck? you can always write comments
    # Try describe the steps you would take manually



if __name__ == "__main__":
    main()

